/*
 * Stores the algorithms of a problem on a parameter.
 * @author vector
 *
 * $Id$
 *
 * This file is part of the Information System on Graph Classes and their
 * Inclusions (ISGCI) at http://www.graphclasses.org.
 * Email: isgci@graphclasses.org
 */

package teo.isgci.problem;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Observable;

import teo.isgci.appl.deducer.complexity.RComplOnClass;
import teo.isgci.appl.deducer.complexity.RComplOnParam;
import teo.isgci.parameter.PseudoClass;

/**
 * Stores the algorithms and deduced complexities on a parameter. Complexities
 * are deduced in multiple steps, see the class Problem.
 */
public class ProblemOnPseudoNode {
    /* The problem. */
    protected Problem problem;
    /* The node. */
    protected PseudoClass node;
    /* The algorithms. */
    protected HashSet<ParamAlgorithm> algos;
    /* The complexitiy of this problem on this pseudoclass. */
    protected ParamComplexity complexity;

    /**
     * Create a new ProblemOnPseudoNode.
     *
     * @param p the Problem to solve
     * @param n the PseudoClass to solve it on
     */
    ProblemOnPseudoNode(Problem p, PseudoClass n) {
        if (!p.validFor(n))
            throw new IllegalArgumentException(p.getName() + " not " +
                    "applicable to " + n.getID());
        problem = p;
        node = n;
        algos = new HashSet<ParamAlgorithm>();
        complexity = ParamComplexity.UNKNOWN;
    }


    /**
     * Update the complexity of this by distilling it with c.
     *
     * @param c the Complexity to set
     * @return whether sth. changed
     */
    protected boolean updateComplexity(ParamComplexity c) throws
            ComplexityClashException {
        c = c.distil(complexity);
        boolean result = !c.equals(complexity);
        complexity = c;
        return result;
    }


    /**
     * Return the complexity of this problem on this pseudonode.
     *
     * @return the comeplexity of this problem on this pseudonode
     */
    ParamComplexity getComplexity() {
        return complexity;
    }




    /**
     * Add an algorithm.
     *
     * @param a the Algorithm to add
     * @return whether sth. changed
     */
    boolean addAlgo(ParamAlgorithm a) {
        algos.add(a);
        try {
            return updateComplexity(a.getComplexity());
        } catch (ComplexityClashException e) {
            System.err.println("Complexity clash for " + problem.getName() +
                    " on " + node + " " + a + " and " + algos);
        }
        return false;
    }


    /**
     * @return the algorithms defined on this node.
     */
    HashSet<ParamAlgorithm> getAlgoSet() {
        return algos;
    }

}

/* EOF */
