/*
 *
 * Deduce complexity considering the co-problem on the co-graph
 *
 * This file is part of the Information System on Graph Classes and their
 * Inclusions (ISGCI) at http://www.graphclasses.org.
 * Email: isgci@graphclasses.org
 */

package teo.isgci.appl.deducer.complexity;

import teo.isgci.appl.deducer.RBoundComplTyping;
import teo.isgci.gc.ComplementClass;
import teo.isgci.gc.GraphClass;
import teo.isgci.problem.Algorithm;
import teo.isgci.problem.Complexity;
import teo.isgci.problem.Problem;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;

@RBoundComplTyping(
        type = ComplementClass.class)

/**
 * Rule 3.6 in parameters.pdf (erweitert)
 * Since co-co-C = C we can simply look, if there has been deduced a
 * complement-complexity
 * for co-C and assign this to C itself.
 * @author JakeM
 */ public class RComplOnClassComplement extends RComplOnClass {

    /**
     * Distribute the Algorithms for this problem over all nodes via the
     * complement.
     * initAlgo/addAlgo must have been called for all problems.
     * distributeAlgorithms must have been called for this problem, and all
     * parent/child problems.
     * Assumes the graph is transitively closed and the complement index is
     * set!
     */
    @Override
    public void run(Problem p, Iterable<GraphClass> classes) {
        if (p.getComplement() == null)
            return;
        boolean result = false;
        GraphClass con;
        Complexity nc, conc;

        for (GraphClass n : classes) {
            if (!(n instanceof ComplementClass))
                continue;
            con = ((ComplementClass) n).getBase();
            nc = p.getDerivedComplexity(n);
            conc = p.getComplement().getDerivedComplexity(con);
            if (!nc.isCompatible(complementComplexity(conc))) {
                System.err.println("ComplexityClash: " +
                        n + " " + p.getName() + "=" + nc + " but " +
                        con + " " + p.getComplement().getName() + "=" + conc);
            } else if (nc.isUnknown() && !conc.isUnknown()) {
                Algorithm algo = getComplementAlgo(complementComplexity
                        (conc), p);
                result |= p.addAlgo(n, algo);
            } else if (conc.isUnknown() && !nc.isUnknown()) {
                Algorithm algo = getComplementAlgo(complementComplexity(nc),
                        p.getComplement());
                result |= p.getComplement().addAlgo(con, algo);
            }
        }
        if (result) {
            RComplOnClass.setChanged(true);
        }
    }

    /**
     * If complement can be solved on co-G in time c, return the time in which
     * this can be solved on G.
     *
     * @return the created algorithm and a Boolean, whether this method
     * changed sth.
     */
    private Complexity complementComplexity(Complexity c) {
        return c.betterThan(Complexity.P) ? Complexity.P : c;
    }

    /**
     * Return an algorithm that solves this on a class in time c, assuming the
     * complement can be solved on co-G.
     *
     * @return the algorithm
     */
    private Algorithm getComplementAlgo(Complexity c, Problem p) {
        final String why = "from " + p.getComplement() + " on the complement";

        if (p.getCoAlgos() == null)
            p.setCoAlgos();
        return getDerivedAlgo(p.getCoAlgos(), c, why, p);
    }

    /**
     * Find in the given list an algorithm of the requested complexity. If it
     * doesn't exist yet, create it node-independently with the given
     * complexity and text.
     *
     * @return the algorithm
     */
    private Algorithm getDerivedAlgo(List<Algorithm> l, Complexity c, String
            why, Problem p) {
        for (Algorithm a : l)
            if (a.getComplexity().equals(c))
                return a;

        Algorithm algo = p.createAlgo(null, c, why).second;
        l.add(algo);
        return algo;
    }
}
