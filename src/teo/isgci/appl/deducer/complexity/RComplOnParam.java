/*
 * Abstract class for rules of complexities on params.
 *
 * This file is part of the Information System on Graph Classes and their
 * Inclusions (ISGCI) at http://www.graphclasses.org.
 * Email: isgci@graphclasses.org
 */

package teo.isgci.appl.deducer.complexity;

import teo.isgci.gc.GraphClass;
import teo.isgci.problem.Problem;


/**
 * A rule that deduces complexities and algorithms dependent on boundedness
 * of certain parameters.
 * This class has to observe all ProblemOnPseudoNode instances.
 *
 * @author JakeM
 */
public abstract class RComplOnParam {


    /**
     * true, if there has been deduced something new.
     */
    private static boolean changed;

    /**
     * Run this rule on the given problem.
     *
     * @param p the problem for which complexities and algorithms are deduced.
     */
    public abstract void run(Problem p, Iterable<GraphClass> classes);

    public static boolean hasChanged() {
        return RComplOnParam.changed;
    }

    public static void setChanged(boolean change) {
        RComplOnParam.changed = change;
    }
}

/* EOF */

