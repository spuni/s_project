/*
 * Stores the algorithms of a problem on a node.
 *
 * $Id$
 *
 * This file is part of the Information System on Graph Classes and their
 * Inclusions (ISGCI) at http://www.graphclasses.org.
 * Email: isgci@graphclasses.org
 */

package teo.isgci.problem;

import java.util.ArrayList;
import java.util.HashSet;

import teo.isgci.gc.GraphClass;

/**
 * Stores the algorithms and deduced complexities on a node. Complexities are
 * deduced in multiple steps, see the class Problem.
 */
public class ProblemOnNode {
    /**
     * The problem
     */
    protected Problem problem;
    /**
     * The node
     */
    protected GraphClass node;
    /**
     * The algorithms
     */
    protected HashSet<Algorithm> algos;
    /**
     * The complexity of this problem on this class.
     */
    protected Complexity complexity;

    ProblemOnNode(Problem p, GraphClass n) {
        if (!p.validFor(n))
            throw new IllegalArgumentException(p.getName() +
                    " not applicable to " + n.getID());
        problem = p;
        node = n;
        algos = new HashSet<Algorithm>();
        complexity = Complexity.UNKNOWN;
    }


    /**
     * Update the complexity of this by distilling it with c.
     *
     * @return whether sth. changed
     */
    protected boolean updateComplexity(Complexity c) throws
            ComplexityClashException {
        c = c.distil(complexity);
        boolean result = !c.equals(complexity);
        complexity = c;
        return result;
    }


    /**
     * Return the complexity at the given step.
     */
    Complexity getComplexity() {
        return complexity;
    }


    /**
     * Add an algorithm at the given deduction step.
     *
     * @return whether sth. changed
     */
    boolean addAlgo(Algorithm a) {
        algos.add(a);
        try {
            return updateComplexity(a.getComplexity());
        } catch (ComplexityClashException e) {
            System.err.println("Complexity clash for " + problem.getName() +
                    " on " + node + " " + a + " and " + algos);
        }
        return false;
    }


    /**
     * Return the algorithms defined on this node.
     */
    HashSet<Algorithm> getAlgoSet() {
        return algos;
    }

}

/* EOF */
