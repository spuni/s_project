/*
 *
 * Move complexity up to union nodes.
 *
 * This file is part of the Information System on Graph Classes and their
 * Inclusions (ISGCI) at http://www.graphclasses.org.
 * Email: isgci@graphclasses.org
 */

package teo.isgci.appl.deducer.complexity;

import teo.isgci.appl.deducer.RBoundComplTyping;
import teo.isgci.gc.GraphClass;
import teo.isgci.gc.UnionClass;
import teo.isgci.problem.Complexity;
import teo.isgci.problem.Problem;
import teo.isgci.problem.Recognition;

import java.util.Iterator;

@RBoundComplTyping(
        type = UnionClass.class)

/**
 * Rule 3.5 in parameters.pdf (erweitert)
 * If we can solve the problem for every part of the
 * union in polytime, then we can apply all part algorithms in polytime,
 * and check their solutions in polytime. So the problem is solvable in
 * polytime on the union.
 * @author JakeM
 */ public class RComplOnClassUnion extends RComplOnClass {

    @Override
    public void run(Problem p, Iterable<GraphClass> classes) {
        if (p.distributeUpUnion(classes)) {
            RComplOnClass.setChanged(true);
        }
    }

//    /**
//     * Try moving complexity information UP to union nodes. We only change
// the
//     * complexity class for Union nodes, and do not generate new
// references or
//     * timebounds.
//     * The reasoning is: If we can solve the problem for every part of the
//     * union in polytime, then we can apply all part algorithms in polytime,
//     * and check their solutions in polytime. So the problem is solvable in
//     * polytime on the union.
//     */
//
//    private void distributedefault(Problem p) {
//        boolean ok;
//
//        for (GraphClass n: p.getGraph().vertexSet()) {
//            if ( !(n instanceof UnionClass) ||
//                    p.getDerivedComplexity(n).betterOrEqual(Complexity.P) )
//                continue;
//
//            //---- Check whether all parts are in P ----
//            ok = true;
//            for (GraphClass part : ((UnionClass) n).getSet()) {
//                if (!p.getDerivedComplexity(part).betterOrEqual(Complexity
// .P)) {
//                    ok = false;
//                    break;
//                }
//            }
//
//            if (ok) {
//                p.createAlgo(n, Complexity.P, "From the constituent
// classes.");
//            }
//        }
//    }

//    /**
//     * Try moving complexity information UP to union nodes. We only change
// the
//     * complexity class for Union nodes, and do not generate new
// references or
//     * timebounds.
//     * The reasoning is: If we can recognize every part of the union in
//     * polytime/lin, then we can take the disjunction of their results in
//     * polytime/lin as well. So the union is recognizable in polytime/lin.
//     */
//    private void distributeRecognition(Recognition p) {
//        boolean ok, linear;
//        Complexity c;
//
//        for (GraphClass n : p.getGraph().vertexSet()) {
//            if ( !(n instanceof UnionClass) ||
//                    p.getDerivedComplexity(n).betterOrEqual(Complexity
// .LINEAR) )
//                continue;
//
//            //---- Check whether all parts are in P ----
//            ok = true;
//            linear = true;
//            Iterator<GraphClass> parts = ((UnionClass) n).getSet()
// .iterator();
//            while (ok  &&  parts.hasNext()) {
//                GraphClass part = parts.next();
//                c = p.getDerivedComplexity(part);
//                ok = ok && c.betterOrEqual(Complexity.P);
//                linear = linear && c.betterOrEqual(Complexity.LINEAR);
//            }
//
//            if (ok  &&  (linear ||
//                    !p.getDerivedComplexity(n).betterOrEqual(Complexity.P)
// )) {
//                //System.err.println("NOTE: distributeUpUnion invoked on "+
//                //n.getName()+" "+toString());
//                p.createAlgo(n,
//                        linear ? Complexity.LINEAR : Complexity.P,
//                        "From the constituent classes.");
//            }
//        }
//    }
}
