/*
 *
 * Move complexity up if we have a reduction.
 *
 * This file is part of the Information System on Graph Classes and their
 * Inclusions (ISGCI) at http://www.graphclasses.org.
 * Email: isgci@graphclasses.org
 */

package teo.isgci.appl.deducer.complexity;

import teo.isgci.gc.GraphClass;
import teo.isgci.problem.Algorithm;
import teo.isgci.problem.Complexity;
import teo.isgci.problem.Problem;
import teo.isgci.problem.Reduction;

import java.util.AbstractMap;

/**
 * Rule 3.4 in parameters.pdf (erweitert)
 * If we can reduce a problem p on a problem q (p has complexity >= GIC,NPC,
 * ...),
 * then q gets same complexity as p.
 * Notice that p is the child, while q is the parent.
 *
 * @author JakeM
 */
public class RComplOnClassReductionChildren extends RComplOnClass {

    /**
     * Distribute the algorithms for the children to this problem.
     *
     * @param p the problem p to which another problem gets reduced (its child)
     */
    @Override
    public void run(Problem p, Iterable<GraphClass> classes) {
        Complexity c;
        boolean result = false;

        for (GraphClass n : classes) {
            if (!n.isPseudoClass()) {
                for (Reduction r : p.getChildren()) {
                    c = r.fromChild(getProgeniallyDerivedComplexity(n, r
                            .getChild()));
                    if (!c.isUnknown()) {
                        Algorithm algo = r.getParentAlgo(c);
                        result |= p.addAlgo(n, algo);
                    }

                }
            }
        }
        if (result) {
            RComplOnClass.setChanged(true);
        }
    }

    /**
     * Get the complexity of n, consulting the child problem, too.
     *
     * @param n the graphclass
     * @param p the problem p to which another problem gets reduced (its child)
     */
    private Complexity getProgeniallyDerivedComplexity(GraphClass n, Problem
            p) {
        if (n.isPseudoClass())
            throw new UnsupportedOperationException("Derived complexity for " +
                    "parameter pseudoclasses" + " is not defined.");

        Complexity c = p.getDerivedComplexity(n);
        if (p.getChildren().isEmpty())
            return c;

        Complexity cc;
        for (Reduction r : p.getChildren()) {
            cc = r.fromChild(getProgeniallyDerivedComplexity(n, r.getChild()));
            if (!cc.isCompatible(c))
                throw new Error("Inconsistent data for " + n + " " + p
                        .getName());
            if (cc.betterThan(c))
                c = cc;
        }
        return c;
    }
}
