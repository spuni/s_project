/*
 *
 * Move complexity down to intersection nodes.
 *
 * This file is part of the Information System on Graph Classes and their
 * Inclusions (ISGCI) at http://www.graphclasses.org.
 * Email: isgci@graphclasses.org
 */

package teo.isgci.appl.deducer.complexity;

import teo.isgci.appl.deducer.RBoundComplTyping;
import teo.isgci.gc.ComplementClass;
import teo.isgci.gc.GraphClass;
import teo.isgci.gc.IntersectClass;
import teo.isgci.problem.Complexity;
import teo.isgci.problem.Problem;
import teo.isgci.problem.Recognition;

import java.util.Iterator;
import java.util.Observable;

@RBoundComplTyping(
        type = IntersectClass.class)

/**
 * Move complexities down to intersection nodes
 */ public class RComplOnClassIntersect extends RComplOnClass {


    @Override
    public void run(Problem p, Iterable<GraphClass> classes) {
        if (p.distributeDownIntersect(classes)) {
            RComplOnClass.setChanged(true);
        }
    }

//    /**
//     * Try moving complexity information DOWN to intersection nodes. We only
//     * change the complexity class for Intersect nodes, and do not
// generate new
//     * references or timebounds.
//     * The reasoning is: If we can recognize every part of the
// intersection in
//     * polytime/lin, then we can take their conjunction in polytime/lin as
//     * well.
//     */
//    private void distributeRecognition(Problem p) {
//        boolean ok, linear;
//        Complexity c;
//
//        for (GraphClass n : p.getGraph().vertexSet()) {
//            if ( !(n instanceof IntersectClass) ||
//                    p.getDerivedComplexity(n).betterOrEqual(Complexity
// .LINEAR) )
//                continue;
//
//            //---- Check whether all parts are in P ----
//            ok = true;
//            linear = true;
//            Iterator<GraphClass> parts =
//                    ((IntersectClass) n).getSet().iterator();
//            while (ok  &&  parts.hasNext()) {
//                GraphClass part = parts.next();
//                c = p.getDerivedComplexity(part);
//                ok = ok && c.betterOrEqual(Complexity.P);
//                linear = linear && c.betterOrEqual(Complexity.LINEAR);
//            }
//
//            if (ok  &&  (linear ||
//                    !p.getDerivedComplexity(n).betterOrEqual(Complexity.P)
// )) {
//                p.createAlgo(n,
//                        linear ? Complexity.LINEAR : Complexity.P,
//                        "From the constituent classes.");
//            }
//        }
//    }

}


/* EOF */