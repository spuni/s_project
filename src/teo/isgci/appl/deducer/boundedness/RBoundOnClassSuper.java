/*
 *
 * Distribute boundedness consulting the superparameters.
 *
 * This file is part of the Information System on Graph Classes and their
 * Inclusions (ISGCI) at http://www.graphclasses.org.
 * Email: isgci@graphclasses.org
 */

package teo.isgci.appl.deducer.boundedness;

import org.jgrapht.DirectedGraph;
import teo.isgci.gc.GraphClass;
import teo.isgci.grapht.GAlg;
import teo.isgci.parameter.Boundedness;
import teo.isgci.parameter.BoundednessProof;
import teo.isgci.parameter.GraphParameter;
import teo.isgci.parameter.PseudoClass;
import teo.isgci.relation.Inclusion;


public class RBoundOnClassSuper extends RBoundOnClass {
    /**
     * Distribute the proofs for the parameters that bound this parameter to
     * this parameter.
     */
    @Override
    public void run(GraphParameter p, Iterable<GraphClass> classes) {
        boolean result = false;
        Boundedness b;
        DirectedGraph<GraphClass, Inclusion> graph = p.getGraph();

        for (GraphClass n : classes) {
            if (!n.isPseudoClass()) {
                for (GraphClass par : GAlg.inNeighboursOf(graph, p
                        .getPseudoclass())) {
                    b = fromSuper(getSuperDerivedBoundedness(n, (
                            (PseudoClass) par).getParameter()));
                    if (!b.isUnknown()) {
                        BoundednessProof proof = p.getSubProof(b, (
                                (PseudoClass) par).getParameter());
                        result |= p.addProof(n, proof);
                    }
                }
            }
        }
        if (result) {
            RBoundOnClass.setChanged(true);
        }
    }

    /**
     * Get the boundedness of n, consulting the superparameter, too. (Rule 4
     * parameters.pdf)
     *
     * @param n the GraphClass for which we want to get data
     * @return the deduced boundedness by parameters which bound this parameter
     */
    private Boundedness getSuperDerivedBoundedness(GraphClass n,
                                                   GraphParameter p) {
        if (n.isPseudoClass())
            throw new UnsupportedOperationException("Boundedness for " +
                    "parameter pseudoclasses is not defined.");

        Boundedness b = p.getDerivedBoundedness(n);
        if (!GAlg.inNeighboursOf(p.getGraph(), p.getPseudoclass()).hasNext())
            return b;

        Boundedness sb;
        for (Object par : GAlg.inNeighboursOf(p.getGraph(), p.getPseudoclass
                ())) {
            sb = fromSuper(((PseudoClass) par).getParameter()
                    .getDerivedBoundedness(n));
            if (!b.isCompatible(sb))
                throw new Error("Inconsistent data for " + n + " " + p
                        .getName());
            if (sb.betterThan(b))
                b = sb;
        }
        return b;
    }

    /**
     * If the superparameter has boundedness b on a graph class, return the
     * boundedness for this class. (Rule 4 parameters.pdf)
     *
     * @param b the boundedness of the superparameter.
     * @return the resulting boundedness.
     */
    private Boundedness fromSuper(Boundedness b) {
        if (b.betterOrEqual(Boundedness.BOUNDED))
            return b;
        return Boundedness.UNKNOWN;
    }
}
