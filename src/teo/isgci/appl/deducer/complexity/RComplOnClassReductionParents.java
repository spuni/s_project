/*
 *
 * Move complexity down if we have a reduction.
 *
 * This file is part of the Information System on Graph Classes and their
 * Inclusions (ISGCI) at http://www.graphclasses.org.
 * Email: isgci@graphclasses.org
 */

package teo.isgci.appl.deducer.complexity;

import teo.isgci.gc.GraphClass;
import teo.isgci.problem.Algorithm;
import teo.isgci.problem.Complexity;
import teo.isgci.problem.Problem;
import teo.isgci.problem.Reduction;

import java.util.AbstractMap;

/**
 * Rule 3.3 in parameters.pdf (erweitert)
 * If we can reduce a problem p on a problem q (q has complexity <= P),
 * then p gets complexity = max(complexity of q, complexity of reduction).
 * Notice that p is the child, while q is the parent.
 *
 * @author JakeM
 */
public class RComplOnClassReductionParents extends RComplOnClass {

    /**
     * Distribute the algorithms for the parents to this problem.
     *
     * @param p the problem p that gets reduced to another problem (its parent)
     */
    @Override
    public void run(Problem p, Iterable<GraphClass> classes) {
        Complexity c;
        boolean result = false;

        for (GraphClass n : classes) {
            if (!n.isPseudoClass()) {
                for (Reduction r : p.getParents()) {
                    c = r.fromParent(getParentallyDerivedComplexity(n, r
                            .getParent()));
                    if (!c.isUnknown()) {
                        Algorithm algo = r.getChildAlgo(c);
                        result |= p.addAlgo(n, algo);
                    }

                }
            }
        }
        if (result) {
            RComplOnClass.setChanged(true);
        }
    }

    /**
     * Get the complexity of n, consulting the parent problem, too.
     *
     * @param n the graphclass
     * @param p the problem p that gets reduced to another problem (its parent)
     */
    private Complexity getParentallyDerivedComplexity(GraphClass n, Problem
            p) {
        if (n.isPseudoClass())
            throw new UnsupportedOperationException("Derived complexity for " +
                    "parameter pseudoclasses" + " is not defined.");

        Complexity c = p.getDerivedComplexity(n);
        if (p.getParents().isEmpty())
            return c;

        Complexity pc;
        for (Reduction r : p.getParents()) {
            pc = r.fromParent(getParentallyDerivedComplexity(n, r.getParent
                    ()));
            if (!c.isCompatible(pc))
                throw new Error("Inconsistent data for " + n + " " + p
                        .getName());
            if (pc.betterThan(c))
                c = pc;
        }
        return c;
    }
}
