/*
 *
 * Move complexity to super and sub parameters.
 *
 * This file is part of the Information System on Graph Classes and their
 * Inclusions (ISGCI) at http://www.graphclasses.org.
 * Email: isgci@graphclasses.org
 */

package teo.isgci.appl.deducer.complexity;

import teo.isgci.appl.deducer.RBoundComplTyping;
import teo.isgci.gc.GraphClass;
import teo.isgci.grapht.GAlg;
import teo.isgci.parameter.PseudoClass;
import teo.isgci.problem.*;

import java.util.*;

@RBoundComplTyping(
        type = PseudoClass.class)

/**
 * Rule 6/7 in parameters.pdf
 * moves Complexities and Algorithms to sub and super parameters.
 * @author JakeM
 */ public class RComplOnParamUpDown extends RComplOnParam {


    /**
     * Distribute the Algorithms for this problem over all nodes.
     * initAlgo/addAlgo must have been called for all problems.
     * Assumes the graph is transitively closed
     */
    @Override
    public void run(Problem p, Iterable<GraphClass> classes) {
        if (p.distributeAlgorithmsPseudoClass(classes)) {
            RComplOnParam.setChanged(true);
        }
    }

//    /**
//     * Distribute the Algorithms for this problem over all nodes.
//     * initAlgo/addAlgo must have been called for all problems.
//     * Assumes the graph is transitively closed
//     */
//    @Override
//    public void run(Problem p, Iterable<GraphClass> classes){
//        ParamComplexity pc;
//        Map<GraphClass,Set<GraphClass> > scc = GAlg.calcSCCMap(p.getGraph());
//
//
//        //---- Add every set of algorithms to the super/subnodes' set. ----
//        for (GraphClass n : p.getGraph().vertexSet()) {
//            if (n.isPseudoClass()) {
//                HashSet<ParamAlgorithm> algos = p.getAlgoSet((PseudoClass)
// n);
//                if (algos == null)
//                    continue;
//
//                PseudoClass pc1 = (PseudoClass) n;
//                pc = p.getDerivedComplexity(pc1);
//                if (pc.distributesMixed()) {
//                    HashSet<ParamAlgorithm> algosUp = new HashSet<>();
//                    HashSet<ParamAlgorithm> algosDown = new HashSet<>();
//                    for (ParamAlgorithm a : algos) {
//                        if (a.getComplexity().distributesUp())
//                            algosUp.add(a);
//                        if (a.getComplexity().distributesDown())
//                            algosDown.add(a);
//                    }
//                    p.distributeParams(algosUp, GAlg.inNeighboursOf(p
// .getGraph(), pc1));
//                    p.distributeParams(algosDown, GAlg.outNeighboursOf(p
// .getGraph(), pc1));
//                } else if (pc.distributesUp()) {
//                    p.distributeParams(algos, GAlg.inNeighboursOf(p
// .getGraph(), pc1));
//                } else if (pc.distributesDown())
//                    p.distributeParams(algos, GAlg.outNeighboursOf(p
// .getGraph(), pc1));
//                else if (pc.distributesEqual())
//                    p.distributeParams(algos, scc.get(pc1));
//            }
//        }
//    }


}


/* EOF */