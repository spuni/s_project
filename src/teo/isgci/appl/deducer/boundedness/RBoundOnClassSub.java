/*
 *
 * Distribute boundedness consulting the subparameters.
 *
 * This file is part of the Information System on Graph Classes and their
 * Inclusions (ISGCI) at http://www.graphclasses.org.
 * Email: isgci@graphclasses.org
 */

package teo.isgci.appl.deducer.boundedness;

import org.jgrapht.DirectedGraph;
import teo.isgci.gc.GraphClass;
import teo.isgci.grapht.GAlg;
import teo.isgci.parameter.Boundedness;
import teo.isgci.parameter.BoundednessProof;
import teo.isgci.parameter.GraphParameter;
import teo.isgci.parameter.PseudoClass;
import teo.isgci.relation.Inclusion;


public class RBoundOnClassSub extends RBoundOnClass {
    /**
     * Distribute the proofs for the parameters that bound this parameter to
     * this parameter.
     */
    @Override
    public void run(GraphParameter p, Iterable<GraphClass> classes) {
        boolean result = false;
        Boundedness b;
        DirectedGraph<GraphClass, Inclusion> graph = p.getGraph();

        for (GraphClass n : classes) {
            if (!n.isPseudoClass()) {
                for (GraphClass par : GAlg.outNeighboursOf(graph, p
                        .getPseudoclass())) {
                    b = fromSub(getSubDerivedBoundedness(n, ((PseudoClass)
                            par).getParameter()));
                    if (!b.isUnknown()) {
                        BoundednessProof proof = p.getSuperProof(b, (
                                (PseudoClass) par).getParameter());
                        result |= p.addProof(n, proof);
                    }
                }
            }
        }
        if (result) {
            RBoundOnClass.setChanged(true);
        }
    }

    /**
     * Get the boundedness of n, consulting the subparameter, too. (Rule 5
     * parameters.pdf)
     *
     * @param n the GraphClass for which we want to get data
     * @param p graph parameter
     * @return the deduced boundedness by parameters which bound this parameter
     */
    private Boundedness getSubDerivedBoundedness(GraphClass n,
                                                 GraphParameter p) {
        if (n.isPseudoClass())
            throw new UnsupportedOperationException("Boundedness for " +
                    "parameter pseudoclasses is not defined.");

        Boundedness b = p.getDerivedBoundedness(n);
        if (!GAlg.outNeighboursOf(p.getGraph(), p.getPseudoclass()).hasNext())
            return b;

        Boundedness sb;
        for (GraphClass par : GAlg.outNeighboursOf(p.getGraph(), p
                .getPseudoclass())) {
            sb = fromSub(((PseudoClass) par).getParameter()
                    .getDerivedBoundedness(n));
            if (!b.isCompatible(sb))
                throw new Error("Inconsistent data for " + n + " " + p
                        .getName());
            if (sb.betterThan(b))
                b = sb;
        }
        return b;
    }

    /**
     * If the subparameter has boundedness b on a graph class, return the
     * boundedness for this class. (Rule 5 parameters.pdf)
     *
     * @param b the boundedness of the subparameter.
     * @return the resulting boundedness.
     */
    private Boundedness fromSub(Boundedness b) {
        if (b.isUnbounded())
            return b;
        return Boundedness.UNKNOWN;
    }
}
