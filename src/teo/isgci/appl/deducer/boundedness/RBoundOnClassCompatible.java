/*
 * Determines boundedness by assuming bounded and trying to find a
 * contradiction.
 *
 * This file is part of the Information System on Graph Classes and their
 * Inclusions (ISGCI) at http://www.graphclasses.org.
 * Email: isgci@graphclasses.org
 */

package teo.isgci.appl.deducer.boundedness;

import teo.isgci.gc.GraphClass;
import teo.isgci.parameter.Boundedness;
import teo.isgci.parameter.BoundednessProof;
import teo.isgci.parameter.GraphParameter;
import teo.isgci.problem.AbstractProblem;
import teo.isgci.problem.Complexity;
import teo.isgci.problem.ParamComplexity;
import teo.isgci.problem.Problem;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;

/**
 * Rule 10 from parameters.pdf. If there is a conflict between
 * the parametrized algorithms and the other ones, the parameter
 * is unbounded.
 */
public class RBoundOnClassCompatible extends RBoundOnClass {


    @Override
    public void run(GraphParameter gp, Iterable<GraphClass> classes) {
        boolean result = false;
        Boundedness b;

        List<Complexity> conflicts = new ArrayList<>();

        for (GraphClass n : classes) {
            if (n.isPseudoClass())
                continue;

            b = gp.getDerivedBoundedness(n);
            if (b.isUnknown() || b.equals(Boundedness.UNBOUNDED)) {
                for (Problem p : AbstractProblem.getProblems()) {
                    conflicts.clear();
                    b = getProblemDerivedBoundedness(n, p, conflicts, gp);
                    if (b.equals(Boundedness.UNBOUNDED)) {// b = unbounded
                        // iff conflicts not empty.
                        BoundednessProof proof = gp.getProblemProof(b, p,
                                conflicts);
                        result |= gp.addProof(n, proof);
                    }

                }
            }
        }
        if (result) {
            RBoundOnClass.setChanged(true);
        }

    }


    /**
     * Get the boundedness of n, consulting problems, too (Rule 10,
     * parameters.pdf). If there is a conflict, unbounded is returned and
     * the conflicting complexities are added to result.
     *
     * @param n the GraphClass for which we want to get data
     * @param p the Problem that is consulted
     * @return unbounded, if there are incompatible complexities for p and the
     * derived boundedness in the current step else
     */
    private Boundedness getProblemDerivedBoundedness(GraphClass n, Problem
            p, List<Complexity> result, GraphParameter gp) {
        if (n.isPseudoClass())
            throw new UnsupportedOperationException("Boundedness for " +
                    "parameter pseudoclasses is not defined.");

//        Boundedness b = gp.getDerivedBoundedness(n);
//        if (!b.isUnknown())
//            return b;

        Complexity c = p.getDerivedComplexity(n);
        ParamComplexity parc = p.getDerivedComplexity(gp.getPseudoClass());
        if (!parc.isCompatible(c)) {
            result.add(parc.toComplexity());
            result.add(c);
            return Boundedness.UNBOUNDED;
        }

        return Boundedness.UNKNOWN;
    }
}

/* EOF */
