/*
 * Abstract class for complexity on graphclass rules.
 *
 * This file is part of the Information System on Graph Classes and their
 * Inclusions (ISGCI) at http://www.graphclasses.org.
 * Email: isgci@graphclasses.org
 */

package teo.isgci.appl.deducer.complexity;

import teo.isgci.gc.GraphClass;
import teo.isgci.problem.Problem;


/**
 * A rule that deduces complexities and algorithms on graph classes.
 * This class has to observe all ProblemOnNode instances.
 *
 * @author JakeM
 */
public abstract class RComplOnClass {


    /**
     * true, if there has been deduced something new.
     */
    private static boolean changed;

    /**
     * Run this rule on the given problem.
     *
     * @param p the problem for which complexities and algorithms are deduced.
     */
    public abstract void run(Problem p, Iterable<GraphClass> classes);

    public static boolean hasChanged() {
        return RComplOnClass.changed;
    }

    public static void setChanged(boolean change) {
        RComplOnClass.changed = change;
    }
}

/* EOF */

