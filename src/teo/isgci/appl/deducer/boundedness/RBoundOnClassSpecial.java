package teo.isgci.appl.deducer.boundedness;

import teo.isgci.gc.GraphClass;
import teo.isgci.gc.ProbeClass;
import teo.isgci.parameter.Boundedness;
import teo.isgci.parameter.Cliquewidth;
import teo.isgci.parameter.GraphParameter;

/**
 * Space for special implemenatations.
 * @author JakeM
 */
public class RBoundOnClassSpecial extends RBoundOnClass {

    /**
     * Do special deductions for a particular parameter.
     * Default implementation does nothing.
     */
    @Override
    public void run(GraphParameter gp) {
        if (gp instanceof Cliquewidth){ distributeSpecial_Clique(gp); }
    }

    protected void distributeSpecial_Clique(GraphParameter gp) {
        for (GraphClass n : gp.getGraph().vertexSet()) {
            if ( !(n instanceof ProbeClass) ||
                    gp.getDerivedBoundedness(n).betterOrEqual(
                            Boundedness.BOUNDED) )
                continue;

            GraphClass base = ((ProbeClass) n).getBase();
            if (gp.getDerivedBoundedness(base).betterOrEqual(Boundedness.BOUNDED))
                gp.createProof(n, Boundedness.BOUNDED, "From the base class.");
        }
    }
}
