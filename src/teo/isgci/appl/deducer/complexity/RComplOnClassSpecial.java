package teo.isgci.appl.deducer.complexity;

import teo.isgci.problem.Problem;

/**
 * Space for special implemenatations.
 * @author JakeM
 */
public class RComplOnClassSpecial extends RComplOnClass{

    /**
     * Do special deductions for a particular problem.
     * Default implementation does nothing.
     */
    @Override
    public void run(Problem p) {

    }
}
