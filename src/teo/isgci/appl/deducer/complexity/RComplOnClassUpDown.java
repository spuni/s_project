/*
 *
 * Move complexity to super- and subnodes.
 *
 * This file is part of the Information System on Graph Classes and their
 * Inclusions (ISGCI) at http://www.graphclasses.org.
 * Email: isgci@graphclasses.org
 */

package teo.isgci.appl.deducer.complexity;

import teo.isgci.gc.ForbiddenClass;
import teo.isgci.gc.GraphClass;
import teo.isgci.grapht.GAlg;
import teo.isgci.problem.Algorithm;
import teo.isgci.problem.Complexity;
import teo.isgci.problem.Problem;
import teo.isgci.problem.Recognition;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Rule 3.1/3.2 and Recognition-Version in parameters.pdf (erweitert)
 * moves Complexities and Algorithms to sub and super classes.
 *
 * @author JakeM
 */
public class RComplOnClassUpDown extends RComplOnClass {


    @Override
    public void run(Problem p, Iterable<GraphClass> classes) {
        if (p.distributeAlgorithmsNormalClass(classes)) {
            RComplOnClass.setChanged(true);
        }
    }

//    /**
//     * Distribute the Algorithms for this problem over all nodes.
//     * initAlgo/addAlgo must have been called for all problems.
//     * Assumes the graph is transitively closed
//     */
//    private void distributedefault(Problem p) {
//        Complexity c;
//        Map<GraphClass,Set<GraphClass> > scc = GAlg.calcSCCMap(p.getGraph());
//        for (GraphClass n : p.getGraph().vertexSet()) {
//            if (!n.isPseudoClass()) {
//                HashSet<Algorithm> algos = p.getAlgoSet(n);
//                if (algos == null)
//                    continue;
//
//                c = p.getDerivedComplexity(n);
//                if (c.distributesUp())
//                    p.distribute(algos, GAlg.inNeighboursOf(p.getGraph(),
// n));
//                else if (c.distributesDown())
//                    p.distribute(algos, GAlg.outNeighboursOf(p.getGraph(),
// n));
//                else if (c.distributesEqual())
//                    p.distribute(algos, scc.get(n));
//            }
//        }
//    }

//    /**
//     * Distribute the Algorithms for this problem over all nodes.
//     * initAlgo/addAlgo must have been called for all problems.
//     * We must redefine this function because Recognition distributes neither
//     * upwards nor downwards.
//     * Assumes the graph g is transitively closed!
//     */
//    private void distributeRecognition(Recognition p) {
//        Map<GraphClass,Set<GraphClass> > scc = GAlg.calcSCCMap(p.getGraph());
//
//        //---- Assert finite ForbiddenClass polynomial
//        if (!p.firstDistributeDone) {
//            for (GraphClass n : p.getGraph().vertexSet()) {
//                if (n instanceof ForbiddenClass &&
//                        ((ForbiddenClass) n).isFinite()) {
//                    p.createAlgo(n, Complexity.P,
//                            "Finite forbidden subgraph characterization");
//                }
//            }
//        }
//
//        //---- Repeat twice in case of an intersection of unions or the other
//        // way around
//        /*for (int repeat = 0; repeat < 2; repeat++)*/ {
//            //---- Add every set of algorithms to the equivalent nodes' set.
//            for (GraphClass n : p.getGraph().vertexSet()) {
//                HashSet h = p.getAlgoSet(n);
//                if (h != null) {
//                    p.distribute(h, scc.get(n));
//                }
//            }
//
//        }
//        p.firstDistributeDone=true;
//    }


}
