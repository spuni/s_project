/*
 * Abstract class for boundedness on graphclass rules.
 *
 * This file is part of the Information System on Graph Classes and their
 * Inclusions (ISGCI) at http://www.graphclasses.org.
 * Email: isgci@graphclasses.org
 */

package teo.isgci.appl.deducer.boundedness;

import teo.isgci.gc.GraphClass;
import teo.isgci.parameter.GraphParameter;


/**
 * A rule that deduces boundednesses of parameters.
 *
 * @author JakeM
 */
public abstract class RBoundOnClass {


    /**
     * true, if there has been deduced something new.
     */
    private static boolean changed;

    public static boolean hasChanged() {
        return RBoundOnClass.changed;
    }

    public static void setChanged(boolean change) {
        RBoundOnClass.changed = change;
    }

    /**
     * Run this rule on the given parameter.
     *
     * @param p the parameter for which boundedness is deduced.
     */
    public abstract void run(GraphParameter p, Iterable<GraphClass> classes);
}

/* EOF */

