/*
 * Stores the boundedness proofs of a parameter on a node.
 * @author vector
 *
 * $Id$
 *
 * This file is part of the Information System on Graph Classes and their
 * Inclusions (ISGCI) at http://www.graphclasses.org.
 * Email: isgci@graphclasses.org
 */

package teo.isgci.parameter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Observable;

import teo.isgci.appl.deducer.boundedness.RBoundOnClass;
import teo.isgci.appl.deducer.complexity.RComplOnClass;
import teo.isgci.gc.GraphClass;
import teo.isgci.problem.ComplexityClashException;

/**
 * Stores the boundedness proofs and deduced boundedness values on a node.
 * Boundedness values are deduced in multiple steps, see the class Parameter.
 */
public class ParameterOnNode {
    /* The parameter. */
    protected GraphParameter parameter;
    /* The node. */
    protected GraphClass node;
    /* The boundedness proofs. */
    protected HashSet<BoundednessProof> proofs;
    /* The boundedness value of this parameter on this class. */
    protected Boundedness boundedness;

    /**
     * Create a new ParameterOnNode with given data
     *
     * @param p the GraphParameter for which this is defined
     * @param n the GraphClass this stores boundedness information for
     */
    ParameterOnNode(GraphParameter p, GraphClass n) {
        if (!p.validFor(n))
            throw new IllegalArgumentException(p.getName() + " not " +
                    "applicable to " + n.getID());
        parameter = p;
        node = n;
        proofs = new HashSet<BoundednessProof>();
        boundedness = Boundedness.UNKNOWN;
    }

    /**
     * Update the boundedness of this by distilling it with b.
     *
     * @param b the boundedness to update to
     * @return whether sth. changed
     */
    protected boolean updateBoundedness(Boundedness b) throws
            ComplexityClashException {
        b = b.distil(boundedness);
        boolean result = !b.equals(boundedness);
        boundedness = b;
        return result;
    }


    /**
     * Return the boundedness of this parameter on this class.
     *
     * @return the boundedness of this parameter on this class
     */
    Boundedness getBoundedness() {
        return boundedness;
    }


    /**
     * Add a proof.
     *
     * @param b the proof to add
     * @return whether sth. changed
     */
    boolean addProof(BoundednessProof b) {
        proofs.add(b);
        try {
            return updateBoundedness(b.getBoundedness());
        } catch (ComplexityClashException e) {
            System.err.println("Complexity clash for " + parameter.getName()
                    + " on " + node + " " + b + " and " + proofs);
        }
        return false;
    }

    /**
     * Return the proofs defined on this node.
     *
     * @return a set of proofs for this ParameterOnNode
     */
    HashSet<BoundednessProof> getProofSet() {
        return proofs;
    }


}

/* EOF */
