/*
 * Problems on graphs.
 *
 * $Id$
 *
 * This file is part of the Information System on Graph Classes and their
 * Inclusions (ISGCI) at http://www.graphclasses.org.
 * Email: isgci@graphclasses.org
 */

package teo.isgci.problem;

import java.util.*;

import org.jgrapht.DirectedGraph;
import teo.isgci.appl.deducer.complexity.RComplOnParam;
import teo.isgci.grapht.*;
import teo.isgci.gc.*;
import teo.isgci.parameter.*;
import teo.isgci.relation.*;
import teo.isgci.ref.*;
import teo.isgci.util.Pair;


/**
 * Stores the information about a graph problem.
 */
public class Problem extends AbstractProblem {

    /**
     * Sparse problems have data for only a few classes
     */
    protected boolean sparse;
    /**
     * Is this Problem applicable to parameters? (added by vector)
     */
    protected boolean forparams;
    /**
     * Stores complexity information on a graph class
     */
    protected Annotation<GraphClass, Inclusion, Complexity> complexAnn;
    /**
     * Stores algorithms on a graph class
     */
    protected Annotation<GraphClass, Inclusion, ProblemOnNode> algoAnn;
    /**
     * Stores complexity information on a graph parameter. (added by vector)
     */
    protected Annotation<GraphClass, Inclusion, ParamComplexity> parComplexAnn;
    /**
     * Stores algorithms on a graph parameter. (added by vector)
     */
    protected Annotation<GraphClass, Inclusion, ProblemOnPseudoNode>
            parAlgoAnn;
    /**
     * More/less general problems
     */
    protected List<Reduction> parents;
    protected List<Reduction> children;
    /**
     * Solving this on G is polytime equivalent to solving complement on co-G
     */
    protected Problem complement;
    /**
     * The algorithms (node independent) that solve depending on co-G.
     */
    protected List<Algorithm> coAlgos;
    /**
     * The algorithms (node independent) that solve this decomposition problem
     * depending on the corresponding parameter. (added by vector)
     */
    protected Map<GraphParameter, Set<Algorithm>> paramDecompAlgos;
    /**
     * The algorithms (node independent) that solve this problem depending on
     * parameters and decomposition complexities. (added by vector)
     */
    protected Map<GraphParameter, Map<Complexity, Set<Algorithm>>> paramAlgos;

    /**
     * The FTP-Algorithms that were resolved from an FPT-lin algo on a
     * parameter. (added by vector)
     */
    protected Map<PseudoClass, Set<ParamAlgorithm>> FPTAlgos;

    protected Problem(String name, DirectedGraph<GraphClass, Inclusion> g) {
        this(name, g, null, null, false);
    }

    /**
     * Create a problem.
     *
     * @param name       the name of the problem ("Independent set")
     * @param g          the graph of classes for which the problem exists
     * @param complement the complement of the problem (Clique)
     * @param directed   to which graphs the problem applies (null for all)
     */
    protected Problem(String name, DirectedGraph<GraphClass, Inclusion> g,
                      Problem complement, GraphClass.Directed directed,
                      boolean forparams) {
        super(name, g, directed);
        if (directed == GraphClass.Directed.PARAMETER)
            this.forparams = true;
        else
            this.forparams = forparams;
        this.sparse = false;
        setComplement(complement);
        this.parents = new ArrayList<Reduction>();
        this.children = new ArrayList<Reduction>();
        this.complexAnn = new Annotation<GraphClass, Inclusion, Complexity>(g);
        this.algoAnn = deducing ? new Annotation<GraphClass, Inclusion,
                ProblemOnNode>(g) : null;
        this.parComplexAnn = new Annotation<GraphClass, Inclusion,
                ParamComplexity>(g);
        this.parAlgoAnn = deducing ? new Annotation<GraphClass, Inclusion,
                ProblemOnPseudoNode>(g) : null;
        this.coAlgos = null;
        this.paramAlgos = new HashMap<GraphParameter, Map<Complexity,
                Set<Algorithm>>>();
        this.paramDecompAlgos = new HashMap<GraphParameter, Set<Algorithm>>();
        this.FPTAlgos = new HashMap<PseudoClass, Set<ParamAlgorithm>>();
    }

    public void setSparse() {
        sparse = true;
    }

    public boolean isSparse() {
        return sparse;
    }

    @Override
    public void setDirected(GraphClass.Directed d) {
        this.directed = d;
        if (d == GraphClass.Directed.PARAMETER)
            forparams = true; // added by vector
    }

    /**
     * Set this problem to be applicable for parameters.
     *
     * @param params a boolean to set the forparams-attribute to
     * @author vector
     */
    public void setParameters(boolean params) {
        forparams = params;
    }

    /**
     * @return true iff this problem is applicable to parameters.
     * @author vector
     */
    public boolean forParameters() {
        return directed == GraphClass.Directed.PARAMETER || forparams;
    }

    /**
     * @return coAlgos
     */
    public List<Algorithm> getCoAlgos() {
        return coAlgos;
    }

    /**
     * generate empty list of coAlgos
     */
    public void setCoAlgos() {
        this.coAlgos = new ArrayList<Algorithm>();
    }

    /**
     * @return variable paramDecompAlgos
     */
    public Map<GraphParameter, Set<Algorithm>> getParamDecompAlgos() {
        return paramDecompAlgos;
    }

    /**
     * @return variable paramAlgos
     */
    public Map<GraphParameter, Map<Complexity, Set<Algorithm>>>
    getParamAlgos() {
        return paramAlgos;
    }

    /**
     * @return children of this problem
     */
    public List<Reduction> getChildren() {
        return children;
    }

    /**
     * @return parents of this problem
     */
    public List<Reduction> getParents() {
        return parents;
    }

    /**
     * Return true iff this problem is applicable to the given class.
     */
    public boolean validFor(GraphClass gc) {
        if (gc.isPseudoClass()) // added by vector
            return forParameters() && (((PseudoClass) gc).getParameter()
                    .forDirected() == this.forDirected() || ((PseudoClass)
                    gc).getParameter().forUndirected() == this.forUndirected
                    ());
        return super.validFor(gc);
    }

    /**
     * @return variable algoAnn
     * @author JakeM
     */
    public Annotation<GraphClass, Inclusion, ProblemOnNode> getAlgoAnn() {
        return algoAnn;
    }

    /**
     * @return variable parAlgoAnn
     * @author JakeM
     */
    public Annotation<GraphClass, Inclusion, ProblemOnPseudoNode> getParAlgoAnn() {
        return parAlgoAnn;
    }

    /**
     * @return variable paramDecompAlgos
     * @author JakeM
     */
    public Map<GraphParameter, Set<Algorithm>> getParamDecompAlgos() {
        return paramDecompAlgos;
    }

    /**
     * @return variable paramAlgos
     * @author JakeM
     */
    public Map<GraphParameter, Map<Complexity, Set<Algorithm>>> getParamAlgos() {
        return paramAlgos;
    }

    /**
     * @return variable parents
     * @author JakeM
     */
    public List<Reduction> getParents() {
        return parents;
    }

    /**
     * @return variable children
     * @author JakeM
     */
    public List<Reduction> getChildren() {
        return children;
    }

    /**
     * @return variable coAlgos
     * @author JakeM
     */
    public List<Algorithm> getCoAlgos() {
        return coAlgos;
    }

    /**
     * @author JakeM
     */
    public void setCoAlgos(List<Algorithm> coAlgos) {
        this.coAlgos = coAlgos;
    }

    /**
     * Adds a new reduction parent->this with cost c.
     */
    public void addReduction(Problem parent, Complexity c) {
        Reduction r = new Reduction(this, parent, c);
        this.parents.add(r);
        parent.children.add(r);
    }

    /**
     * Return the reductions from other problems to this.
     */
    public Iterator<Reduction> getReductions() {
        return parents.iterator();
    }


    public void setComplement(Problem thecomplement) {
        this.complement = thecomplement;
        if (thecomplement != null)
            thecomplement.complement = this;
    }

    public Problem getComplement() {
        return complement;
    }

    /**
     * Create a new problem with the given name and graph;
     */
    public static Problem createProblem(String name,
                                        DirectedGraph<GraphClass, Inclusion>
                                                g) {
        Problem p = null;
        if (name.equals("Recognition"))
            p = new Recognition(name, g);
        else
            p = new Problem(name, g);
        if (deducing)
            problems.add(p);
        return p;
    }

    //====================== Complexity on a node ===========================

    /**
     * Return the stored complexity of this problem on n. Return UNKNOWN if
     * nothing is stored.
     */
    public Complexity getComplexity(GraphClass n) {
        Complexity c = complexAnn.getNode(n);
        return c == null ? Complexity.UNKNOWN : c;
    }


    /**
     * Return the stored complexity of this problem on the PseudoClass n.
     * Return UNKNOWN if nothing is stored.
     *
     * @param n the pseudoclass to get the Complexity for
     * @return a ParamComplexity
     * @author vector
     */
    public ParamComplexity getComplexity(PseudoClass n) {
        ParamComplexity c = parComplexAnn.getNode(n);
        return c == null ? ParamComplexity.UNKNOWN : c;
    }


    /**
     * Set the complexity of this on n to c.
     */
    public void setComplexity(GraphClass n, Complexity c) {
        if (n.isPseudoClass())
            throw new UnsupportedOperationException("P/NP complexity on a "
                    + "parameter.");
        complexAnn.setNode(n, c);
    }

    /**
     * Set the complexity of this on pseudoclass n to c.
     *
     * @param n the PseudoClass to set the complexity for
     * @param c the ParamComplexity for this on n
     * @author vector
     */
    public void setComplexity(PseudoClass n, ParamComplexity c) {
        parComplexAnn.setNode(n, c);
    }

    /**
     * Return the complexity of this problem on n, as derived in the last
     * completed step.
     * Meant to be used internally and for XML writing.
     */
    public Complexity getDerivedComplexity(GraphClass n) {
        if (n.isPseudoClass())
            throw new UnsupportedOperationException("Derived complexity for " +
                    "parameter pseudoclasses" + " is not defined.");
        ProblemOnNode pon = algoAnn.getNode(n);

        return pon == null ? Complexity.UNKNOWN : pon.getComplexity();
    }

    /**
     * Return the complexity of this problem on pseudoclass n, as derived in
     * the last completed step. Meant to be used internally and for XML
     * writing.
     *
     * @param n the PseudoClass to get the derived Complexity for
     * @return the ParamComplexity of this on n
     * @author vector
     */
    public ParamComplexity getDerivedComplexity(PseudoClass n) {
        ProblemOnPseudoNode pon = parAlgoAnn.getNode(n);

        return pon == null ? ParamComplexity.UNKNOWN : pon.getComplexity();
    }


    //============================ Algorithms =============================


    /**
     * Add an algorithm for this problem on graphclass n and update the
     * complexity on n.
     *
     * @return whether sth. changed
     */
    public boolean addAlgo(GraphClass n, Algorithm a) {
        if (!graph.containsVertex(n) || n.isPseudoClass())
            throw new IllegalArgumentException("Invalid node");
        if (a.getProblem() != this && a.getProblem() != complement)
            throw new IllegalArgumentException("Invalid algorithm " + a);

        ProblemOnNode pon = algoAnn.getNode(n);
        if (pon == null) {
            pon = new ProblemOnNode(this, n);
            algoAnn.setNode(n, pon);
        }

        return pon.addAlgo(a);
    }

    /**
     * Add an algorithm for this problem on graphclass n and update the
     * complexity on pseudoclass n.
     *
     * @param n the PseudoClass to add an algorithm for
     * @param a the algortithm for this on the PseudoClass
     * @return whether sth. changed.
     * @author vector
     */
    public boolean addAlgo(PseudoClass n, ParamAlgorithm a) {
        if (!graph.containsVertex(n))
            throw new IllegalArgumentException("Invalid node");
        if (a.getProblem() != this && a.getProblem() != complement)
            throw new IllegalArgumentException("Invalid algorithm " + a);

        ProblemOnPseudoNode pon = parAlgoAnn.getNode(n);
        if (pon == null) {
            pon = new ProblemOnPseudoNode(this, n);
            parAlgoAnn.setNode(n, pon);
        }

        return pon.addAlgo(a);
    }

    /**
     * Add all algorithms in iter to n.
     *
     * @return whether sth. changed
     */
    public boolean addAlgos(GraphClass n, Iterator<Algorithm> iter) {
        boolean result = false;
        while (iter.hasNext()) {
            result |= addAlgo(n, iter.next());
        }
        return result;
    }

    /**
     * Add all algorithms in iter to pseudoclass n.
     *
     * @param n    the PseudoClass to add algorithms for
     * @param iter the Algorithms to add
     * @return whether sth. changed
     * @author vector
     */
    public boolean addAlgos(PseudoClass n, Iterator<ParamAlgorithm> iter) {
        boolean result = false;
        while (iter.hasNext()) {
            ParamAlgorithm a = iter.next();
            if (a.getComplexity().equals(ParamComplexity.FPTLIN)) {
                a = getFPTAlgo(a.getComplexity(), a.getPseudoClass());
            }
            result |= addAlgo(n, a);
        }
        return result;
    }

    /**
     * Get a FPT-Algorithm for a FPT-lin-Algorithm.
     *
     * @param c    the ParamComplexity to get an algorithm for
     * @param from the PseudoClass on which the FPT-lin algorithm is defined
     * @return the FPT-Algortihm either from the FPTAlgos-List or a new one
     * @author vector
     */
    public ParamAlgorithm getFPTAlgo(ParamComplexity c, PseudoClass from) {
        String why = "from " + c.getComplexityString() + " on " + from;
        if (FPTAlgos.get(from) == null)
            FPTAlgos.put(from, new HashSet<ParamAlgorithm>());
        for (ParamAlgorithm a : FPTAlgos.get(from)) {
            if (a.getComplexity().equals(ParamComplexity.FPT))
                return a;
        }

        ParamAlgorithm algo = createAlgo(null, ParamComplexity.FPT, why)
                .second;
        FPTAlgos.get(from).add(algo);
        return algo;
    }

    /**
     * Create a new algorithm for this problem on a node n, add it to node n
     * and return it.
     * n may be null.
     *
     * @return Pair consisting of boolean, whether this method changed sth.
     * and the created algorithm
     */
    public Pair<Boolean, Algorithm> createAlgo(GraphClass n, Complexity
            complexity, String bounds, List refs) {
        boolean result = false;
        Algorithm res = new SimpleAlgorithm(this, n, complexity, bounds, refs);
        if (n != null)
            result |= addAlgo(n, res);
        return new Pair<>(new Boolean(result), res);
    }

    /**
     * Create a new algorithm for this problem on a pseudoclass n, add it to
     * pseudoclass n and return it. n may be null.
     *
     * @param n          the PseudoClass to create an algorithm for
     * @param complexity the complexity class of the algorithm
     * @param refs       the references for the algorithm
     * @return Pair consisting of boolean, whether this method changed sth.
     * and the created algorithm
     * @author vector
     */
    public Pair<Boolean, ParamAlgorithm> createAlgo(PseudoClass n,
                                                    ParamComplexity
                                                            complexity,
                                                    String bounds, List refs) {
        boolean result = false;
        ParamAlgorithm res = new ParamAlgorithm(this, n, complexity, bounds,
                refs);
        if (n != null)
            result |= addAlgo(n, res);
        return new Pair<>(new Boolean(result), res);
    }

    /**
     * Create a new algorithm for this problem on a node n with a simple
     * explanation (Note text), add it to node n and return it.
     * n may be null.
     *
     * @return Pair consisting of boolean, whether this method changed sth.
     * and the created algorithm
     */
    public Pair<Boolean, Algorithm> createAlgo(GraphClass n, Complexity
            complexity, String why) {
        List refs = new ArrayList();
        refs.add(new Note(why, null));
        return createAlgo(n, complexity, null, refs);
    }

    /**
     * Create a new algorithm for this problem on a pseudoclass n with a simple
     * explanation (Note text), add it to pseudoclass n and return it. n may be
     * null.
     *
     * @param n          the PseudoClass to create an algorithm for
     * @param complexity the complexity class of the algorithm
     * @param why        a text describing the algorithm
     * @return Pair consisting of boolean, whether this method changed sth.
     * and the created algorithm
     * @author vector
     */
    public Pair<Boolean, ParamAlgorithm> createAlgo(PseudoClass n,
                                                    ParamComplexity
                                                            complexity,
                                                    String why) {
        List refs = new ArrayList();
        refs.add(new Note(why, null));
        return createAlgo(n, complexity, null, refs);
    }

    /**
     * Get the algorithms for this problem that work on node n or null if there
     * are none.
     */
    public HashSet<Algorithm> getAlgoSet(GraphClass n) {
        if (algoAnn == null)
            return null;

        ProblemOnNode pon = algoAnn.getNode(n);
        return pon == null ? null : pon.getAlgoSet();
    }

    /**
     * Get the algorithms for this problem that work on pseudoclass n or null
     * if there are none.
     *
     * @param n the PseudoClass to get the AlgoSet for
     * @return the set of Algorithms for n
     * @author vector
     */
    public HashSet<ParamAlgorithm> getAlgoSet(PseudoClass n) {
        if (parAlgoAnn == null)
            return null;

        ProblemOnPseudoNode pon = parAlgoAnn.getNode(n);
        return pon == null ? null : pon.getAlgoSet();
    }

    /**
     * Return an iterator over the algorithms for this problem on the given
     * node. Never returns null.
     */
    public Iterator<Algorithm> getAlgos(GraphClass n) {
        HashSet<Algorithm> hash = getAlgoSet(n);
        if (hash == null)
            hash = new HashSet<Algorithm>();
        return hash.iterator();
    }

    /**
     * Return an iterator over the algorithms for this problem on the given
     * pseudoclass. Never returns null.
     *
     * @param n the PseudoClass to get the AlgoSet for
     * @return the iterator of Algorithms for n
     * @author vector
     */
    public Iterator<ParamAlgorithm> getAlgos(PseudoClass n) {
        HashSet<ParamAlgorithm> hash = getAlgoSet(n);
        if (hash == null)
            hash = new HashSet<ParamAlgorithm>();
        return hash.iterator();
    }


    //====================== Distribution of algorithms =====================


    /**
     * Distribute the Algorithms for this problem over all normal nodes.
     * initAlgo/addAlgo must have been called for all problems.
     * Assumes the graph is transitively closed!
     *
     * @return whether this method changed sth.
     */
    public boolean distributeAlgorithmsNormalClass(Iterable<GraphClass>
                                                           classes) {
        boolean result = false;
        Complexity c;
        Map<GraphClass, Set<GraphClass>> scc = GAlg.calcSCCMap(graph);

        //---- Add every set of algorithms to the super/subnodes' set. ----
        for (GraphClass n : classes) {
            if (!n.isPseudoClass()) {
                HashSet<Algorithm> algos = getAlgoSet(n);
                if (algos == null)
                    continue;

                c = getDerivedComplexity(n);
                if (c.distributesUp())
                    result |= distribute(algos, GAlg.inNeighboursOf(graph, n));
                else if (c.distributesDown())
                    result |= distribute(algos, GAlg.outNeighboursOf(graph,
                            n));
                else if (c.distributesEqual())
                    result |= distribute(algos, scc.get(n));
            }
        }
        return result;
    }

    /**
     * Distribute the Algorithms for this problem over all pseudonodes.
     * initAlgo/addAlgo must have been called for all problems.
     * Assumes the graph is transitively closed!
     *
     * @return whether this method changed sth.
     */
    public boolean distributeAlgorithmsPseudoClass(Iterable<GraphClass>
                                                           classes) {
        boolean result = false;
        ParamComplexity pc;
        Map<GraphClass, Set<GraphClass>> scc = GAlg.calcSCCMap(graph); //
        // probably double runtime?

        //---- Add every set of algorithms to the super/subnodes' set. ----
        for (GraphClass n : classes) {
            if (n.isPseudoClass()) { // added by vector
                HashSet<ParamAlgorithm> algos = getAlgoSet((PseudoClass) n);
                if (algos == null)
                    continue;

                PseudoClass p = (PseudoClass) n;
                pc = getDerivedComplexity(p);
                if (pc.distributesMixed()) {
                    HashSet<ParamAlgorithm> algosUp = new HashSet<>();
                    HashSet<ParamAlgorithm> algosDown = new HashSet<>();
                    for (ParamAlgorithm a : algos) {
                        if (a.getComplexity().distributesUp())
                            algosUp.add(a);
                        if (a.getComplexity().distributesDown())
                            algosDown.add(a);
                    }
                    result |= distributeParams(algosUp, GAlg.inNeighboursOf
                            (graph, p));
                    result |= distributeParams(algosDown, GAlg
                            .outNeighboursOf(graph, p));
                } else if (pc.distributesUp()) {
                    result |= distributeParams(algos, GAlg.inNeighboursOf
                            (graph, p));
                } else if (pc.distributesDown())
                    result |= distributeParams(algos, GAlg.outNeighboursOf
                            (graph, p));
                else if (pc.distributesEqual())
                    result |= distributeParams(algos, scc.get(p));
            }
        }
        return result;
    }


    /**
     * Try moving complexity information UP to union nodes. We only change the
     * complexity class for Union nodes, and do not generate new references or
     * timebounds.
     * The reasoning is: If we can solve the problem for every part of the
     * union in polytime, then we can apply all part algorithms in polytime,
     * and check their solutions in polytime. So the problem is solvable in
     * polytime on the union.
     *
     * @return whether this method changed sth.
     */
    public boolean distributeUpUnion(Iterable<GraphClass> classes) {
        boolean result = false;
        int i;
        boolean ok;
        Complexity c;

        for (GraphClass n : classes) {
            if (!(n instanceof UnionClass) || getDerivedComplexity(n)
                    .betterOrEqual(Complexity.P))
                continue;

            //---- Check whether all parts are in P ----
            ok = true;
            for (GraphClass part : ((UnionClass) n).getSet()) {
                if (!getDerivedComplexity(part).betterOrEqual(Complexity.P)) {
                    ok = false;
                    break;
                }
            }

            if (ok) {
                //System.err.println("NOTE: distributeUpUnion invoked on "+
                //n.getName()+" "+toString());
                result |= createAlgo(n, Complexity.P, "From the constituent " +
                        "" + "classes.").first;
            }
        }
        return result;
    }


    /**
     * Try moving complexity information DOWN to intersection nodes.
     * Example: If we can recognize every part of the intersection in
     * polytime/lin, then we can take their conjunction in polytime/lin as
     * well.
     *
     * @return false, because method did nothing.
     */
    public boolean distributeDownIntersect(Iterable<GraphClass> classes) {
        return false;
    }


    /**
     * Adds the algorithms in algos to the classes in nodes.
     *
     * @return whether sth. changed
     */
    public boolean distribute(HashSet<Algorithm> algos, Iterable<GraphClass>
            nodes) {
        boolean result = false;
        for (GraphClass n : nodes) {
            result |= addAlgos(n, algos.iterator());
        }
        return result;
    }

    /**
     * Adds the algorithms in algos to the pseudoclasses in nodes.
     *
     * @return whether this method changed sth.
     */
    public boolean distributeParams(HashSet<ParamAlgorithm> algos,
                                    Iterable<GraphClass> nodes) {
        boolean result = false;
        for (GraphClass n : nodes) {
            result |= addAlgos((PseudoClass) n, algos.iterator());
        }
        return result;
    }

    /**
     * Call this before reading the graph when you're going to deduce.
     */
    public static void setDeducing() {
        deducing = true;
        problems = new ArrayList<Problem>();
    }


}


/* EOF */
