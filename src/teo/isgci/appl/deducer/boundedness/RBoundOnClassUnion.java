/*
 *Boundedness on the union comes from boundedness of all classes in the union.
 *
 * This file is part of the Information System on Graph Classes and their
 * Inclusions (ISGCI) at http://www.graphclasses.org.
 * Email: isgci@graphclasses.org
 */

package teo.isgci.appl.deducer.boundedness;

import teo.isgci.appl.deducer.RBoundComplTyping;
import teo.isgci.gc.GraphClass;
import teo.isgci.gc.UnionClass;
import teo.isgci.parameter.Boundedness;
import teo.isgci.parameter.GraphParameter;

@RBoundComplTyping(
        type = UnionClass.class)

/**
 * This class implements rule number 1 in Section 1 from JakeM's Document
 * Rules on parameters in the appendix.
 * The rule says: If a parameter gamma is bounded on Graphclasses c_1, c_2,
 * ... c_n
 * then gamma is also bound on the union
 */ public class RBoundOnClassUnion extends RBoundOnClass {
    @Override
    public void run(GraphParameter gp, Iterable<GraphClass> classes) {
        boolean result = false;
        boolean ok;


        for (GraphClass graphclass : classes) {
            if (!(graphclass instanceof UnionClass) || gp
                    .getDerivedBoundedness(graphclass).betterThan
                            (Boundedness.BOUNDED))
                continue;

            // Check whether all parts are bounded --
            ok = true;
            for (GraphClass part : ((UnionClass) graphclass).getSet()) {
                if (!gp.getDerivedBoundedness(part).betterOrEqual
                        (Boundedness.BOUNDED)) {
                    ok = false;
                    break;
                }
            }

            if (ok) {
                result |= gp.createProof(graphclass, Boundedness.BOUNDED,
                        "From the constituent classes.").first;
            }

        }
        if (result) {
            RBoundOnClass.setChanged(true);
        }
    }
}

