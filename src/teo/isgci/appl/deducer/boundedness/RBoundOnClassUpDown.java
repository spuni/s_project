/*
 *
 * Distribute boundedness to super/subclasses aswell.
 *
 * This file is part of the Information System on Graph Classes and their
 * Inclusions (ISGCI) at http://www.graphclasses.org.
 * Email: isgci@graphclasses.org
 */

package teo.isgci.appl.deducer.boundedness;

import org.jgrapht.DirectedGraph;
import teo.isgci.gc.GraphClass;
import teo.isgci.grapht.GAlg;
import teo.isgci.parameter.Boundedness;
import teo.isgci.parameter.BoundednessProof;
import teo.isgci.parameter.GraphParameter;
import teo.isgci.relation.Inclusion;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


public class RBoundOnClassUpDown extends RBoundOnClass {
    /**
     * Distribute the boundedness proofs for this parameter over all nodes.
     * initProof/addProof must have been called for all parameters. Assumes
     * graph is transitively closed!
     */
    @Override
    public void run(GraphParameter p, Iterable<GraphClass> classes) {
        boolean result = false;
        Boundedness b;
        DirectedGraph<GraphClass, Inclusion> graph = p.getGraph();
        Map<GraphClass, Set<GraphClass>> scc = GAlg.calcSCCMap(graph);

        // ---- Add every set of algorithms to the super/subnodes' set. ----
        for (GraphClass n : classes) {
            if (!n.isPseudoClass()) {
                HashSet<BoundednessProof> proofs = p.getProofSet(n);
                if (proofs != null) {
                    b = p.getDerivedBoundedness(n);
                    if (b.distributesUp())
                        result |= distribute(proofs, GAlg.inNeighboursOf
                                (graph, n), p);
                    else if (b.distributesDown())
                        result |= distribute(proofs, GAlg.outNeighboursOf
                                (graph, n), p);
                    else if (b.distributesEqual())
                        result |= distribute(proofs, scc.get(n), p);
                }
            }
        }
        if (result) {
            RBoundOnClass.setChanged(true);
        }
    }

    /**
     * Adds the proofs in proofs to the classes in nodes.
     *
     * @param proofs the proofs to add to the classes
     * @param nodes  the nodes the proofs are added to
     */
    private boolean distribute(HashSet<BoundednessProof> proofs,
                               Iterable<GraphClass> nodes, GraphParameter p) {
        boolean result = false;
        for (GraphClass n : nodes) {
            result |= addProofs(n, proofs.iterator(), p);
        }
        return result;
    }

    /**
     * Add all proofs in iter to n.
     *
     * @param n    the GraphClass to add proofs for
     * @param iter the collection of proofs to add
     */
    private boolean addProofs(GraphClass n, Iterator iter, GraphParameter p) {
        boolean result = false;
        while (iter.hasNext()) {
            result |= p.addProof(n, (BoundednessProof) iter.next());
        }
        return result;
    }

}
