/*
 * Defines a special rule for the parameter Cliquewidth
 *
 * This file is part of the Information System on Graph Classes and their
 * Inclusions (ISGCI) at http://www.graphclasses.org.
 * Email: isgci@graphclasses.org
 */
package teo.isgci.appl.deducer.boundedness;


import teo.isgci.gc.GraphClass;
import teo.isgci.gc.ProbeClass;
import teo.isgci.parameter.Boundedness;
import teo.isgci.parameter.Cliquewidth;
import teo.isgci.parameter.GraphParameter;

public class RBoundOnClassCliquewidth extends RBoundOnClass {

    /**
     * special rule for cliquewidth
     *
     * @param gp graph parameter
     */
    public void run(GraphParameter gp, Iterable<GraphClass> classes) {
        boolean result = false;
        if (gp instanceof Cliquewidth) {
            for (GraphClass n : classes) {
                if (!(n instanceof ProbeClass) || gp.getDerivedBoundedness
                        (n).betterOrEqual(Boundedness.BOUNDED))
                    continue;

                GraphClass base = ((ProbeClass) n).getBase();
                if (gp.getDerivedBoundedness(base).betterOrEqual(Boundedness
                        .BOUNDED)) {
                    result |= gp.createProof(n, Boundedness.BOUNDED, "From "
                            + "the base class.").first;
                }
            }
        }
        if (result) {
            RBoundOnClass.setChanged(true);
        }
    }
}
