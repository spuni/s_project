/*
 *
 * Distribute complexities to graphclasses from bounded params.
 *
 * This file is part of the Information System on Graph Classes and their
 * Inclusions (ISGCI) at http://www.graphclasses.org.
 * Email: isgci@graphclasses.org
 */

package teo.isgci.appl.deducer.complexity;

import teo.isgci.gc.GraphClass;
import teo.isgci.parameter.Boundedness;
import teo.isgci.parameter.GraphParameter;
import teo.isgci.parameter.PseudoClass;
import teo.isgci.problem.*;

import java.util.*;

/**
 * Rule 9 from parameters.pdf. Distribute complexities to graph classes
 * from parameters which are bounded on them
 */
public class RComplOnClassFromParam extends RComplOnClass {


    @Override
    public void run(Problem p, Iterable<GraphClass> classes) {
        Complexity c;
        boolean result = false;

        for (GraphClass n : classes) {
            if (!n.isPseudoClass()) {
                for (GraphParameter par : AbstractProblem.getParameters()) {
                    PseudoClass pc = par.getPseudoClass();
                    if (par.getDerivedBoundedness(n).isBounded()) {
                        if (par.isPolyDecomp() && p.equals(par
                                .getDecompositionProblem())) {
                            // Distribute the standard decomposition time for a
                            // bounded parameter.
                            Algorithm algo = getParamDecompAlgo(par
                                    .getDecomposition(), par, Boundedness
                                    .BOUNDED, p);
                            result |= p.addAlgo(n, algo);
                        } else {
                            ParamComplexity parcom = p.getDerivedComplexity
                                    (pc);
                            Complexity decomp = par.isLinDecomp() ?
                                    Complexity.LINEAR : par
                                    .getDecompositionProblem()
                                    .getDerivedComplexity(n);
                            c = getParamResultComplexity(decomp, parcom);
                            if (!c.isUnknown()) {
                                Algorithm algo = getParamAlgo(c, par,
                                        parcom, decomp, p);
                                result |= p.addAlgo(n, algo);
                            }
                        }
                    }
                }
            }
        }
        if (result) {
            RComplOnClass.setChanged(true);
        }
    }


    /**
     * Return an algorithm on a class in time c assuming the parameter is
     * bounded.
     *
     * @param c   the complexity to get an algorithm for
     * @param par the GraphParameter this is the decomposition problem for
     * @param b   the boundedness of the parameter
     * @return an Algorithm solving this in time c
     * @author vector
     */
    private Algorithm getParamDecompAlgo(Complexity c, GraphParameter par,
                                         Boundedness b, Problem p) {
        final String why = "from " + b.getComplexityString() + " " + par
                .getName();
        if (p.getParamDecompAlgos().get(par) == null)
            p.getParamDecompAlgos().put(par, new HashSet<Algorithm>());
        for (Algorithm a : p.getParamDecompAlgos().get(par)) {
            if (a.getComplexity().equals(c))
                return a;
        }

        Algorithm algo = p.createAlgo(null, c, why).second;
        p.getParamDecompAlgos().get(par).add(algo);
        return algo;
    }


    /**
     * Get the resulting complexity given, a decomposition time and a
     * parameterized complexity.
     *
     * @param decomp the complexity for the decomposition problem.
     * @param parcom the parameterized complexity on a bounded parameter.
     * @return the resulting complexity
     * @author vector
     */
    private Complexity getParamResultComplexity(Complexity decomp,
                                                ParamComplexity parcom) {
        Complexity complexity = parcom.toComplexity();
        return decomp.betterThan(complexity) ? complexity : decomp;
    }


    /**
     * Return an algorithm on a class in time c assuming the parameter is
     * bounded.
     *
     * @param c      the Complexity to get an Algotithm for
     * @param par    the GraphParameter the Complexity is derived of
     * @param parcom the ParamComplexity c is derived from
     * @param decomp the decomposition time for par
     * @return an algorithm solving this in time c either from the list or a
     * new one.
     * @author vector
     */
    private Algorithm getParamAlgo(Complexity c, GraphParameter par,
                                   ParamComplexity parcom, Complexity
                                           decomp, Problem p) {
        final String why = "from " + parcom.getComplexityString() + " on " +
                par.getName() + " and " + decomp.getComplexityString() + " " +
                "decomposition time";
        if (p.getParamAlgos().get(par) == null)
            p.getParamAlgos().put(par, new HashMap<Complexity,
                    Set<Algorithm>>());
        if (p.getParamAlgos().get(par).get(decomp) == null)
            p.getParamAlgos().get(par).put(decomp, new HashSet<Algorithm>());
        for (Algorithm a : p.getParamAlgos().get(par).get(decomp)) {
            if (a.getComplexity().equals(c))
                return a;
        }

        Algorithm algo = p.createAlgo(null, c, why).second;
        p.getParamAlgos().get(par).get(decomp).add(algo);
        return algo;
    }


}

/* EOF */