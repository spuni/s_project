/*
 * Boundedness considering the co-parameter on the co-graph
 *
 * This file is part of the Information System on Graph Classes and their
 * Inclusions (ISGCI) at http://www.graphclasses.org.
 * Email: isgci@graphclasses.org
 */

package teo.isgci.appl.deducer.boundedness;

import teo.isgci.appl.deducer.RBoundComplTyping;
import teo.isgci.gc.ComplementClass;
import teo.isgci.gc.GraphClass;
import teo.isgci.parameter.Boundedness;
import teo.isgci.parameter.BoundednessProof;
import teo.isgci.parameter.GraphParameter;

@RBoundComplTyping(
        type = ComplementClass.class)

/**
 * This class implements rule number 2 in section 1 from JakeM's Document
 * Rules on parameters in the appendix.
 * The rule says: Let gamma be a graphparameter and let c be a graphclass.
 * The boundedness of gamma on c equals to the boundedness of co-gamma on co-c
 */ public class RBoundOnClassComplement extends RBoundOnClass {

    /**
     * This method runs the rule for the boundedness on co-gamma on co-c
     *
     * @param gp the graphparameter, that shall be bounded on co-c when it
     *           is bounded on c
     */
    @Override
    public void run(GraphParameter gp, Iterable<GraphClass> classes) {
        boolean result = false;
        GraphParameter complement;
        complement = gp.getComplement();
        if (complement == null)
            return;

        String name = gp.getName();
        String complementName = complement.getName();

        GraphClass con;
        Boundedness nc, conc;

        for (GraphClass graphclass : classes) {
            if (!(graphclass instanceof ComplementClass))
                continue;
            con = ((ComplementClass) graphclass).getBase();
            nc = gp.getDerivedBoundedness(graphclass);
            conc = complement.getDerivedBoundedness(con);

            if (!nc.isCompatible(gp.complementBoundedness(conc))) {
                System.err.println("ComplexityClash: " + graphclass + " " +
                        name + "=" + nc + " but " + con + " " +
                        complementName + "=" + conc);
            } else if (nc.isUnknown() && !conc.isUnknown()) {
                BoundednessProof proof = getComplementProof(gp
                        .complementBoundedness(conc), gp);
                result |= gp.addProof(graphclass, proof);
            } else if (conc.isUnknown() && !nc.isUnknown()) {
                BoundednessProof proof = getComplementProof(complement
                        .complementBoundedness(nc), complement);
                result |= complement.addProof(con, proof);
            }
        }
        if (result) {
            RBoundOnClass.setChanged(true);
        }
    }


    /**
     * Return an proof for boundedness b of this on a class, assuming the
     * complement is bounded on co-G.
     *
     * @param b  the boundedness to get a proof for
     * @param gp the graphparameter that is given through the run-Method
     * @return either a proof from the list of coProofs or a new one
     */
    private BoundednessProof getComplementProof(Boundedness b,
                                                GraphParameter gp) {
        final String why = "from " + gp.getComplement() + " on the complement";

        if (gp.getCoProofs() == null)
            gp.setCoProofs();
        return gp.getDerivedProof(gp.getCoProofs(), b, why);
    }

}
